import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

    static Connection conn;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info?user=admin&password=admin");

        LocalDate N = LocalDate.of(2023, 1, 18);

        String SQL_QUERY = String.format(
                """
                    SELECT last_name, first_name, sur_name
                    FROM   student
                    JOIN   grade_book ON student.id = grade_book.student_id
                    WHERE  grade_book.exam_date < "%s";
                """, N.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        PreparedStatement studentsQuery = conn.prepareStatement(SQL_QUERY);

        ResultSet studentsSet = studentsQuery.executeQuery();

        System.out.println("Last name " + "-".repeat(10) + " Name " + "-".repeat(10) + " Surname");
        while (studentsSet.next()){
            System.out.println(studentsSet.getString(1) + " " +studentsSet.getString(2) + studentsSet.getString(3));
        }
    }
}
